import React from 'react';
import {
    Card,
    H3,
    FormGroup,
    InputGroup,
    Tag,
    RadioGroup,
    Radio,
    Button,
    Elevation,
    Toaster,
    Classes,
    Checkbox,
    Dialog,
    Popover,
    Menu,
    MenuItem,
    Position,
} from '@blueprintjs/core';
import Uploader from 'react-dnd-file-uploader';

import { nearestNeighborScaling, Dimension, getDataURL } from './utils';

import './App.scss';

// ref: http://courses.cs.vt.edu/~masc1044/L17-Rotation/ScalingNN.html
const App: React.FC = () => {
    const [isExportDialogOpen, toggleExportDialogOpen] = React.useState<
        boolean
    >(false);
    const [canvasWrapperDimension, setCanvasWrapperDimension] = React.useState<
        Dimension
    >();
    const canvasWrapperRef = React.useCallback((node: HTMLDivElement) => {
        if (node) {
            const { width, height } = node.getBoundingClientRect();
            setCanvasWrapperDimension({
                width,
                height,
            });
        }
    }, []);
    const [loading, setLoading] = React.useState<boolean>(false);
    const [image, setImage] = React.useState<HTMLImageElement>();
    const [dimension, setDimension] = React.useState<Dimension>();
    const [canvasDimension, setCanvasDimension] = React.useState<Dimension>();

    const calcDimension = (
        wrapperDimension: Dimension,
        imageDimension: Dimension,
    ): Dimension => {
        const { width: wrapperWidth, height: wrapperHeight } = wrapperDimension;
        const { width: imageWidth, height: imageHeight } = imageDimension;

        let width = imageWidth;
        let height = imageHeight;

        const ratio = Math.min(
            wrapperWidth / imageWidth,
            wrapperHeight / imageHeight,
        ); // this step fits the larger part of the image to the canvas

        if (imageWidth > wrapperWidth) {
            width = Math.min(Math.round(imageWidth * ratio), wrapperWidth);
        }

        if (imageHeight > wrapperHeight) {
            height = Math.min(Math.round(imageHeight * ratio), wrapperHeight);
        }

        return {
            width,
            height,
        };
    };

    const render = ({
        canvas,
        image,
        imageDimension: { width, height },
        algorithm,
    }: {
        canvas: HTMLCanvasElement;
        image: HTMLImageElement;
        imageDimension: Dimension;
        algorithm: 'default' | 'nearest-neighbor';
    }): void => {
        const context = canvas.getContext('2d');
        switch (algorithm) {
            case 'default':
                context?.clearRect(0, 0, canvas.width, canvas.height);

                canvas.width = width;
                canvas.height = height;

                context?.drawImage(image, 0, 0, width, height);
                break;

            case 'nearest-neighbor':
                const virtualCanvas = document.createElement('canvas');
                virtualCanvas.width = image.width;
                virtualCanvas.height = image.height;

                const virtualContext = virtualCanvas.getContext('2d');

                virtualContext?.drawImage(
                    image,
                    0,
                    0,
                    image.width,
                    image.height,
                );

                const imageData = virtualContext?.getImageData(
                    0,
                    0,
                    image.width,
                    image.height,
                );

                if (!imageData) return;

                const output = nearestNeighborScaling(imageData, {
                    width,
                    height,
                });
                const outputImageData = new ImageData(output, width, height);

                context?.clearRect(0, 0, canvas.width, canvas.height);

                context?.putImageData(outputImageData, 0, 0);
                break;
        }
    };

    React.useEffect(() => {
        if (!image || !canvasWrapperDimension) return;

        const {
            width: wrapperWidth,
            height: wrapperHeight,
        } = canvasWrapperDimension;
        const { width, height } = image;

        const canvasDimension = calcDimension(
            { width: wrapperWidth, height: wrapperHeight },
            { width, height },
        );

        setCanvasDimension(canvasDimension);

        setDimension({
            width,
            height,
        });
    }, [image, canvasWrapperDimension]);

    React.useEffect(() => {
        if (!image || !dimension || !canvasWrapperDimension) return;

        setLoading(true);
        const {
            width: wrapperWidth,
            height: wrapperHeight,
        } = canvasWrapperDimension;

        const { width: imageWidth, height: imageHeight } = dimension;

        const { width, height } = calcDimension(
            { width: wrapperWidth, height: wrapperHeight },
            { width: imageWidth, height: imageHeight },
        );

        setCanvasDimension({
            width,
            height,
        });

        const canvas = document.getElementById('canvas') as HTMLCanvasElement;

        render({
            canvas,
            image,
            imageDimension: {
                width,
                height,
            },
            algorithm: 'default',
        });

        setCanvasDimension(canvasDimension);
        setLoading(false);
    }, [dimension, canvasWrapperDimension]);

    return (
        <div className="app">
            <div className="left-panel">
                <Card elevation={Elevation.THREE}>
                    <H3>Properties</H3>

                    <div className="properties">
                        <div className="algorithm-select-container property">
                            <RadioGroup
                                label="Algorithm"
                                inline={true}
                                onChange={(): void => {
                                    // to be imprelemnted
                                }}
                                selectedValue="nearest-neighbor"
                            >
                                {['nearest-neighbor'].map((algorithm) => (
                                    <Radio
                                        key={algorithm}
                                        label="Nearest Neighbor"
                                        value={algorithm}
                                    />
                                ))}
                            </RadioGroup>
                        </div>
                        <div className="dimension-input-container property">
                            <FormGroup label="Dimension">
                                <div className="inputs-wrapper">
                                    {['width', 'height'].map((inputName) => {
                                        let value = '';
                                        if (dimension) {
                                            if (
                                                inputName === 'width' &&
                                                dimension.width !== -1
                                            )
                                                value = `${dimension.width}`;
                                            if (
                                                inputName === 'height' &&
                                                dimension.height !== -1
                                            )
                                                value = `${dimension.height}`;
                                        }

                                        return (
                                            <InputGroup
                                                key={`dimension-input-${inputName}`}
                                                id={`${inputName}`}
                                                value={value}
                                                disabled={!image}
                                                className={`dimension-input ${inputName}`}
                                                placeholder={inputName}
                                                rightElement={
                                                    <Tag minimal={true}>px</Tag>
                                                }
                                                onChange={(
                                                    e: React.ChangeEvent<
                                                        HTMLInputElement
                                                    >,
                                                ): void => {
                                                    e.preventDefault();

                                                    if (!dimension) return;

                                                    const { value } = e.target;

                                                    if (!value) {
                                                        setDimension({
                                                            ...dimension,
                                                            [inputName]: -1, // -1 indicates an empty string
                                                        });

                                                        return;
                                                    }

                                                    if (
                                                        /[^0-9]/gi.test(value)
                                                    ) {
                                                        // not allowed
                                                        return;
                                                    }

                                                    const parsedValue = parseInt(
                                                        value,
                                                    );

                                                    setDimension({
                                                        ...dimension,
                                                        [inputName]: parsedValue,
                                                    });
                                                }}
                                            />
                                        );
                                    })}
                                </div>
                            </FormGroup>
                        </div>

                        <div className="aspect-ratio-checker">
                            <Checkbox
                                label="ignore aspect ratio"
                                disabled={true}
                            />
                        </div>
                    </div>

                    <Popover
                        fill={true}
                        position={Position.RIGHT}
                        content={
                            <Menu>
                                <MenuItem
                                    text="JPG"
                                    onClick={(): void => {
                                        if (!image || !dimension) return;

                                        const link = document.createElement(
                                            'a',
                                        );

                                        link.download = 'image.jpeg';
                                        link.href = getDataURL(
                                            image,
                                            {
                                                width: dimension?.width,
                                                height: dimension?.height,
                                            },
                                            'jpeg',
                                        );
                                        link.click();
                                    }}
                                />
                                <MenuItem
                                    text="PNG"
                                    onClick={(): void => {
                                        if (!image || !dimension) return;

                                        const link = document.createElement(
                                            'a',
                                        );

                                        link.download = 'image.png';
                                        link.href = getDataURL(
                                            image,
                                            {
                                                width: dimension?.width,
                                                height: dimension?.height,
                                            },
                                            'png',
                                        );

                                        link.click();
                                    }}
                                />
                            </Menu>
                        }
                    >
                        <Button
                            fill={true}
                            disabled={!image}
                            intent="primary"
                            onClick={(): void => {
                                toggleExportDialogOpen(true);
                            }}
                        >
                            Export
                        </Button>
                    </Popover>
                </Card>
            </div>
            <div className="right-panel">
                <Card
                    className="image-content-card"
                    elevation={Elevation.THREE}
                >
                    {image && (
                        <div
                            ref={canvasWrapperRef}
                            className={`canvas-wrapper ${
                                loading ? Classes.SKELETON : ''
                            }`}
                        >
                            <canvas
                                id="canvas"
                                width={canvasDimension?.width}
                                height={canvasDimension?.height}
                            ></canvas>
                        </div>
                    )}

                    {!image && (
                        <Uploader
                            onFileLoadComplete={(data): void => {
                                if (typeof data !== 'string') {
                                    Toaster.create().show({
                                        message: `It looks like the file is not an image. You can use png or jpeg types. Please try again. If this is not the case, please contact me.`,
                                        intent: 'danger',
                                        icon: 'warning-sign',
                                        timeout: 10000,
                                    });
                                    return;
                                }

                                // base64 image string will be returned
                                const image = new Image();

                                image.onload = (): void => {
                                    setImage(image);
                                };

                                image.src = data as string;
                            }}
                        />
                    )}
                </Card>
            </div>
        </div>
    );
};

export default App;
