export type Dimension = {
    width: number;
    height: number;
};

export type Position = {
    x: number;
    y: number;
};

export type Scale = {
    scaleX: number;
    scaleY: number;
};

export class RGBA {
    public r: number;
    public g: number;
    public b: number;
    public a: number;

    constructor(r: number, g: number, b: number, a: number) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
}

/**
 * ref: http://courses.cs.vt.edu/~masc1044/L17-Rotation/ScalingNN.html
 * @param sourceImage
 * @param outputDimension
 */
export const nearestNeighborScaling = (
    sourceImage: ImageData,
    outputDimension: Dimension,
): Uint8ClampedArray => {
    const outputImageArray: RGBA[] = [];
    const { width, height, data } = sourceImage;
    const imageArray2d = convertImageData(data, { width, height });
    for (let i = 0; i < outputDimension.height; i++) {
        for (let j = 0; j < outputDimension.width; j++) {
            const color = findColorFromSourceImage({
                sourceImage: imageArray2d,
                position: { x: i, y: j } as Position,
                outputImageDimension: outputDimension,
            });
            outputImageArray.push(color);
        }
    }

    const flattened: number[] = [];
    for (const color of outputImageArray) {
        flattened.push(color.r, color.g, color.b, color.a);
    }

    return new Uint8ClampedArray(flattened);
};

/**
 * @param currentPosition
 * @param sourceImage
 * @param scale
 *
 * NOTE:
 * If it tries to extract pixel from sourceImage that does not exists (out of boundary),
 * it will return rgb (0,0,0)
 */
export const findColorFromSourceImage = ({
    sourceImage,
    position,
    outputImageDimension,
}: {
    sourceImage: RGBA[][];
    position: { x: number; y: number };
    outputImageDimension: Dimension;
}): RGBA => {
    const imageDimension: Dimension = {
        width: sourceImage[0].length,
        height: sourceImage.length,
    };

    const scale: Scale = {
        scaleX: imageDimension.width / outputImageDimension.width,
        scaleY: imageDimension.height / outputImageDimension.height,
    };

    const sourceX = Math.min(
        Math.round(position.x * scale.scaleX),
        imageDimension.width - 1,
    );
    const sourceY = Math.min(
        Math.round(position.y * scale.scaleY),
        imageDimension.height - 1,
    );

    // index in 1-D array is
    // height * y + (x * 4)
    // 4 here is RGBA

    return sourceImage[sourceX][sourceY];
};

export const convertImageData = (
    imageData: Uint8ClampedArray,
    dimension: Dimension,
): RGBA[][] => {
    if (!imageData) throw new Error('imageData parameter is invalid');
    if (imageData.length === 0) throw new Error('imageData parameter is empty');
    if (imageData.length !== dimension.width * dimension.height * 4)
        throw new Error(
            `imageData.length should equal to ${
                dimension.width * dimension.height * 4
            }`,
        );
    const result: RGBA[][] = [];
    let row: RGBA[] = [];
    for (let i = 0; i < imageData.length; i += 4) {
        const rgba = new RGBA(
            imageData[i],
            imageData[i + 1],
            imageData[i + 2],
            imageData[i + 3],
        );
        row.push(rgba);
        if (row.length === dimension.width) {
            result.push(row);
            row = [];
        }
    }

    return result;
};

export const getDataURL = (
    image: HTMLImageElement,
    dimension: Dimension,
    imageType: 'jpeg' | 'png',
): string => {
    const canvas = document.createElement('canvas');

    canvas.width = dimension.width;
    canvas.height = dimension.height;

    const context = canvas.getContext('2d');

    context?.drawImage(image, 0, 0, dimension.width, dimension.height);

    if (imageType === 'jpeg') {
        return canvas.toDataURL('image/jpeg', 0.8);
    } else {
        return canvas.toDataURL();
    }
};
