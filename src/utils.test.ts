import { Dimension, findColorFromSourceImage, convertImageData } from './utils';

describe('findColorFromSourceImage', () => {
    const generateRamdomClampedArray = (size: number): number[] => {
        let array: number[] = [];

        for (let i = 0; i < size; i++) {
            const r = Math.random() * 255;
            const g = Math.random() * 255;
            const b = Math.random() * 255;
            const a = 255;

            array = [...array, r, g, b, a];
        }

        return array;
    };

    it('should properly return rgba from source image array', () => {
        const image = new Uint8ClampedArray(generateRamdomClampedArray(4 * 4));
        const sourceImageDimension: Dimension = {
            width: 4,
            height: 4,
        };
        const imageArray2d = convertImageData(image, sourceImageDimension);

        const outputDimension: Dimension = {
            width: 10,
            height: 10,
        };

        // when an image goes from 4x4 to 10x10, the rounded index seq go as follow.
        // output image index -> 0 1 2 3 4 5 6 7 8 9
        // from source image  -> 0 0 1 1 2 2 2 3 3 4
        // these rounded index will be used to get the color from the sourceImage and
        // the last index, 4 will be set to 3 to avoid error.

        // CHECK few random positions
        const foundColor1 = findColorFromSourceImage({
            sourceImage: imageArray2d,
            position: { x: 0, y: 1 },
            outputImageDimension: outputDimension,
        });

        const foundColor2 = findColorFromSourceImage({
            sourceImage: imageArray2d,
            position: { x: 5, y: 7 },
            outputImageDimension: outputDimension,
        });

        const foundColor3 = findColorFromSourceImage({
            sourceImage: imageArray2d,
            position: { x: 7, y: 9 },
            outputImageDimension: outputDimension,
        });

        const expectedColor1 = imageArray2d[0][0];
        const expectedColor2 = imageArray2d[2][3];
        const expectedColor3 = imageArray2d[3][3];

        expect(foundColor1).toEqual(expectedColor1);
        expect(foundColor2).toEqual(expectedColor2);
        expect(foundColor3).toEqual(expectedColor3);
    });
});

describe('convertImageData', () => {
    it('should convert imageData to 2D RGBA', () => {
        const imageData = new Uint8ClampedArray(new Array(4 * 4));

        const image = convertImageData(imageData, { width: 2, height: 2 });

        expect(image).toBeTruthy();
        expect(image.length).toEqual(2);
        expect(image[0].length).toEqual(2);
    });

    it('should handle edge cases where the imageData has invalid size', () => {
        const notValid = null;
        const emptyArray: Uint8ClampedArray = new Uint8ClampedArray([]);
        const tooBig = new Uint8ClampedArray(new Array(4 * 4 * 4));
        const tooSmall = new Uint8ClampedArray(new Array(4));

        const dimension: Dimension = {
            width: 2,
            height: 2,
        };

        expect(() => {
            // eslint-disable-next-line
            convertImageData(notValid as any, dimension);
        }).toThrowError(new Error('imageData parameter is invalid'));
        expect(() => {
            // eslint-disable-next-line
            convertImageData(emptyArray as any, dimension);
        }).toThrowError(new Error('imageData parameter is empty'));

        expect(() => {
            convertImageData(tooBig, dimension);
        }).toThrowError(
            new Error(
                `imageData.length should equal to ${
                    dimension.width * dimension.height * 4
                }`,
            ),
        );
        expect(() => {
            convertImageData(tooSmall, dimension);
        }).toThrowError(
            new Error(
                `imageData.length should equal to ${
                    dimension.width * dimension.height * 4
                }`,
            ),
        );
    });
});
